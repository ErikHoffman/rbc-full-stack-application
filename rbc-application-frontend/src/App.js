import React, { Component } from 'react';
import Fxchange from './components/Fxchange/Fxchange';
import './App.css';

const fxchangeOptions = [
  { value: 'CAD', label: 'CAD', className: 'fxchange-option' }, 
  { value: 'GBP', label: 'GBP', className: 'fxchange-option' }, 
  { value: 'EUR', label: 'EUR', className: 'fxchange-option' }];

const apiUrl = "http://localhost:3001";

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      usdCurrencyConvert: 0,
      
    };
    this.handleCurrencyUpdate = this.handleCurrencyUpdate.bind(this);
    this.setupTimeout = this.setupTimeout.bind(this);
    this.undoTimeout = this.undoTimeout.bind(this);
    this.getExchangeVal = this.getExchangeVal.bind(this);
  }


  /* Handle all of the USD currency functions, these will be passed into the fxchange component 
     as generic functions to be ran on different update events, making it reusable for different functionality later */
  handleCurrencyUpdate(value) {
    this.setState({
      usdCurrencyConvert: value
    })
  }

  /* Set up interval for checking exchange value or close it based on input state */
  setupTimeout(amountValue, selectedCurrency) {
    if (amountValue > 0 && selectedCurrency) {
        if (this.interval) { // If timeout is already running stop it to prevent multiple streams
          clearInterval(this.interval);
        }
        this.interval = setInterval(() => this.getExchangeVal(amountValue), 100);
    } else if (amountValue === '') { // User manually deleted the amount in the input, can remove the call timeout for now
        clearInterval(this.interval);
        this.interval = null;
        this.handleCurrencyUpdate(0);
    }
  }

  /* Simple clear for interval, and update to 0 value */
  undoTimeout() {
    clearInterval(this.interval); 
    this.interval = null;
    this.handleCurrencyUpdate(0);
  }

  componentWillUnmount() {
    clearInterval(this.interval); //On close stop the request interval
  }

  /* Async request to api, do not call if value is 0 */
  async getExchangeVal(amountValue) {
    if (parseFloat(amountValue) !== 0) {
        const usdResponse = await fetch(`${apiUrl}/exchangeValue?value=${amountValue}`)
        const json = await usdResponse.json();
        this.handleCurrencyUpdate(json.usdValue);
    } else {
        this.handleCurrencyUpdate(0);
    }
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <p>
            Erik Hoffman RBC Full Stack Developer Submission:
          </p>

          <Fxchange 
            options={fxchangeOptions} 
            formUpdate={this.setupTimeout}
            handleClearAmount={this.undoTimeout}
          />
          <p>USD conversion amount: ${this.state.usdCurrencyConvert}</p>
        </header>
      </div>
    );
  }
}

export default App;

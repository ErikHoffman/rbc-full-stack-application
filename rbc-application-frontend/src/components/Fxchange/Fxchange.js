import React, { Component } from 'react';
import Select from 'react-select';
import CurrencyFormat from 'react-currency-format';
import './Fxchange.css';

class Fxchange extends Component {
    constructor(props) {
        super(props);
        this.state = {
            amountValue: '',
            formattedValue: '',
            options: props.options,
            selectedCurrency: null
        };

        this.updateAmount = this.updateAmount.bind(this);
        this.clearAmount = this.clearAmount.bind(this);
        this.updateSelectedCurrency = this.updateSelectedCurrency.bind(this);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    updateAmount(values) {
        const {formattedValue, value} = values;
        this.setState({
            amountValue: value,
            formattedValue: formattedValue
        },
        function() {
            this.props.formUpdate(this.state.amountValue, this.state.selectedCurrency);
        }
        );
    }

    // Handle X button press on devices smaller than 360px wide, clears the input field
    clearAmount() { 
        this.setState({
            formattedValue: '',
            amountValue: ''
        }, 
        function() {
            this.props.handleClearAmount();
        }
        );
    }

    updateSelectedCurrency(selectedOption) {
        this.setState({ 
            selectedCurrency: selectedOption 
        },
        function() {
            this.props.formUpdate(this.state.amountValue, this.state.selectedCurrency);
        }
        );
    } 

    render() {
        return (
            <div className="fxchange-parent">
              <CurrencyFormat
                className="fxchange-input"
                value={this.state.formattedValue} 
                placeholder="Enter Amount"
                thousandSeparator={true}
                prefix={'$'}
                decimalScale={2}
                onValueChange={(values) => {
                  this.updateAmount(values)}}
              />
              <Select 
                className="fxchange-select" 
                placeholder="Currency..."
                selectedOption={this.state.selectedCurrency}
                options={this.state.options} 
                onChange={this.updateSelectedCurrency}
              />
              <button 
                className="fxchange-clear-button" 
                onClick={this.clearAmount}>
                X
              </button>
            </div>
        )
    }
}

export default Fxchange;
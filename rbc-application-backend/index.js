var express = require('express');
var app = express();

var exchangeValue = require('./exchangeValue.js');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  });

app.use('/exchangeValue', exchangeValue);

app.get('/', function(req, res) {
    res.send("OK");
});



app.listen(3001);
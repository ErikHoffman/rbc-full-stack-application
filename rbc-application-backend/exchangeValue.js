var express = require('express');
var router = express.Router();

router.get('/', function(req, res) {
    const {value} = req.query;
    res.json({ usdValue: convertToUSD(value)});
});

function convertToUSD(value) {
    return (parseFloat(value) * 2 + parseFloat(Math.random(0, 1))).toFixed(2);
}

module.exports = router;